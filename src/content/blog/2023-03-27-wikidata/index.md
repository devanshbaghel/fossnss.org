---
path: /blog/wikidata
date: "2023-03-27"
datestring: "27 March 2023"
cover: "./poster.jpeg"
tag: "Wikidata"
author: "SravyaPrakasan"
name: "Sravya P"
title: "Workshop on WIKIDATA"
desc: "Celebrating Open Data!"
---


The "Wikidata" workshop conducted on March 27,2023 as part of celebrating Open Data Days from March 4th to March 10th was a full-day hands-on workshop organised by Dyuksha'23 in association with FOSSNSS. The event was held with a motive to promote wikidata, the free and open knowledge project and other wikimedia projects by wikimedia foundation.It brought together a bunch of students who shared a common interest in exploring about open data sources. 

The workshop provided a safe and supportive environment for participants to unleash their creativity through hands-on activities and interactive discussions. The session was engaged by instructors from the Wikimedians of Kerala User Group, Manoj Karingamadathil, Jinoy Tom Jacob, Mujeeb Rahman, Ambady Anand S and Kannan V M who shared information regarding Wikidata and some Wiki tools and gadgets.

Following the introduction,the morning session included the importance of working on Wikimedia projects,importance of structured open data,creating Wikimedia accounts,how to edit Wikidata and an introduction to Wikidata Query Service.The talks on each topic was designed in a way to encourage participants to know more on data contribution by their own. The afternoon session was scheduled with an edit-a-thon workshop.Various Wikidata tools were familiarized through the workshop. Mix'n'Match tool which can list entries of some external databases, and allows users to match them against Wikidata items were used and verifications were made on the entered information.Contributions were made to wikidata project related to details of Railway Stations of Kerala using editing tools. Participants contributing the data and retrieving the data from Wikidata became more interesting. The facilitators provided guidance and encouraged participants to explore their instincts.

One of the highlights of the workshop was the enthusiastic participation from all attendees.The workshop also included interactive discussions where participants could express their thoughts and ask questions.The open and non-judgmental environment created by the facilitators allowed everyone to freely express their thoughts and seek guidance from one another.

The "Wikidata" workshop was a resounding success, providing a platform for participants to explore on open data sources. The engaging activities, interactive discussions, and supportive atmosphere were all inspiring one to contribute more to Wikidata and to be a part of Wikimedia projects. The workshop left participants equipped with new tools and ideas to continue their journeys.
